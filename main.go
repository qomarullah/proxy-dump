package main

import (
	"flag"
	"fmt"
	"net/url"
	"os"
	"strings"

	"log"

	"go.uber.org/zap"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var (
	upstream string
	port     string
)

func init() {
	flag.StringVar(&upstream, "upstream", "http://localhost:80", "the target (<http/s>://<host>:<port>)")
	flag.StringVar(&port, "port", "9090", "the tunnelthing port")

}

func main() {
	flag.Parse()

	envPort := os.Getenv("PORT")
	if envPort != "" {
		port = envPort
	}
	envUpstream := os.Getenv("UPSTREAM")
	if envUpstream != "" {
		upstream = envUpstream
	}

	loggerConfig := zap.NewProductionConfig()
	loggerConfig.EncoderConfig.TimeKey = "timestamp"
	//loggerConfig.EncoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout(time.RFC3339)

	logger, err := loggerConfig.Build()
	if err != nil {
		log.Fatal(err)
	}
	sugar := logger.Sugar()

	e := echo.New()
	e.Use(middleware.Recover())
	//e.Use(middleware.Logger())

	// Setup proxy
	arrayUpstream := strings.Split(upstream, ",")

	upstreams := []*middleware.ProxyTarget{}
	for _, k := range arrayUpstream {
		urlUpstream, err := url.Parse(k)
		if err != nil {
			e.Logger.Fatal(err)
		}
		proxy := middleware.ProxyTarget{URL: urlUpstream}
		upstreams = append(upstreams, &proxy)

		e.Use(middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {
			//fmt.Println("request:", c.Request().RequestURI, c.Request().Header, string(reqBody), ">>", string(resBody))
			sugar.Infow("gateway", zap.String("method", c.Request().Method), zap.String("path", c.Request().URL.Path), zap.String("req", string(reqBody)), zap.String("resp", string(resBody)))

		}))
	}
	e.Use(middleware.Proxy(middleware.NewRoundRobinBalancer(upstreams)))
	fmt.Println("UPSTREAM:", upstream)
	e.Logger.Fatal(e.Start(":" + port))

}
